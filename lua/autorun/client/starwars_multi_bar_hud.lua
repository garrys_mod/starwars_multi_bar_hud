-- Display a message to confirm reading the file.
print("----Starwars multi bar hud----")
print("----Version 1-----")

--[[-------------------------------------------------------------------------
                                Make hud core
---------------------------------------------------------------------------]]

local function hud_left_part()
    --[[---
    Barre du nom 
    -----]]

    draw.RoundedBox(10, 20, ScrH() - 130, 230, 30, Color(15, 15, 15, 250))-- Fond de la barre
    draw.RoundedBox(10, 50, ScrH() - 125, 2, 20, Color(60, 60, 60, 250))-- Barre de séparation
    draw.SimpleText(LocalPlayer():Nick(), "Trebuchet18", 138, ScrH() - 125, color_white, TEXT_ALIGN_CENTER)

    ------ Icon du nom -----
    surface.SetMaterial(Material("materials/idcarte_icon.png"))
    surface.SetDrawColor(255, 255, 255)
    surface.DrawTexturedRect(26, ScrH() - 125, 20, 20)


    --[[---
    Barre du métier 
    -----]]
    --/!\ Nécessite DarkRP ! 

    draw.RoundedBox(10, 20, ScrH() - 85, 230, 30, Color(15, 15, 15, 250))-- Fond de la barre
    draw.RoundedBox(10, 50, ScrH() - 80, 2, 20, Color(60, 60, 60, 250))-- Barre de séparation
    draw.SimpleText(LocalPlayer():getDarkRPVar("job") or "Erreur", "Trebuchet18", 138, ScrH() - 80, color_white, TEXT_ALIGN_CENTER)

    ------ Icon du métier -----
    surface.SetMaterial(Material("materials/job_icon.png"))
    surface.SetDrawColor(255,255,255)
    surface.DrawTexturedRect(25, ScrH() - 80, 23, 20)


    --[[---
    Barre de l'argent 
    -----]]
    --/!\ Nécessite DarkRP ! 

    draw.RoundedBox(10, 20, ScrH() - 40, 230, 30, Color(15, 15, 15, 250))-- Fond de la barre
    draw.RoundedBox(10, 50, ScrH() - 35, 2, 20, Color(60, 60, 60, 250))-- Barre de séparation
    local argent_joueur = DarkRP.formatMoney(LocalPlayer():getDarkRPVar("money")).. " (".. DarkRP.formatMoney(LocalPlayer():getDarkRPVar("salary")).. "/H)" or "Erreur"
    draw.SimpleText(argent_joueur, "Trebuchet18", 138, ScrH() - 35, color_white, TEXT_ALIGN_CENTER) -- Texte argents

    ------ Icon de l'argent -----
    surface.SetMaterial(Material("materials/credit_icon.png"))
    surface.SetDrawColor(255, 255, 255)
    surface.DrawTexturedRect(18, ScrH() - 40, 40, 30)


    --[[---
    Barre de nourriture
    -----]]
    --/!\ Nécessite DarkRP ! 

    local faim = math.ceil(LocalPlayer():getDarkRPVar("Energy") or 100)
    if faim == 100 then return end
    ----- Faim support -----
    draw.RoundedBox(10, 260, ScrH() - 130, 30, 120, Color(15, 15, 15, 250))-- Fond de la barre
    draw.RoundedBox(0, 265, ScrH() - 125, 20, 80, Color(30, 30, 30, 250))-- Fond gris de la barre de faim
    draw.RoundedBox(10, 265, ScrH() - 40, 20, 2, Color(60, 60, 60, 250))-- Barre de séparation
    ----- Faim icon -----
    surface.SetMaterial(Material("materials/food_icon.png"))
    surface.SetDrawColor(255,255,255)
    surface.DrawTexturedRect(264, ScrH() - 36, 23, 23)
    ----- Faim barre -----
    local taille_barre_faim = faim / 100 * 80
    draw.RoundedBox(0, 265, ScrH() - 45 - taille_barre_faim, 20, taille_barre_faim, Color(85, 60, 0, 255)) -- barre de faim.
    draw.SimpleText(faim or "Erreur", "Default", 275, ScrH() - 90, color_white, TEXT_ALIGN_CENTER) -- Nombre faim
end

local function hud_middle_part()
    --[[---
    Barre de vie 
    -----]]
    local taille_barre_vie_base = LocalPlayer():Health() / LocalPlayer():GetMaxHealth() * 200
    local taille_barre_vie = 0
    if taille_barre_vie_base > 200 then
        taille_barre_vie = 200
    else
        taille_barre_vie = taille_barre_vie_base
    end

    draw.RoundedBox(10, ScrW()/2 - 255, ScrH() - 35, 250, 30, Color(15, 15, 15, 250))-- Fond de la barre

    draw.RoundedBox(15, ScrW()/2 - 218, ScrH() - 27, 200, 15, Color(30, 30, 30, 255)) -- Fond de la barre rouge
    draw.RoundedBox(15, ScrW()/2 - 218, ScrH() - 27, taille_barre_vie, 15, Color(200, 0, 0, 255)) -- Rouge de la barre de vie
    draw.SimpleText(LocalPlayer():Health() or "Error", "DermaDefault", ScrW()/2 - 118, ScrH() - 26, color_white, TEXT_ALIGN_CENTER) -- Nombre vie
    draw.RoundedBox(10, ScrW()/2 - 225, ScrH() - 30, 2, 20, Color(60, 60, 60, 250))-- Barre de séparation
    
    -- Icon de vie
    surface.SetMaterial(Material("materials/hp_icon.png"))
    surface.SetDrawColor(255, 255, 255)
    surface.DrawTexturedRect(ScrW()/2 - 248, ScrH() - 30, 20, 20)  


    --[[---
    Barre d'armure
    -----]]

    local taille_barre_armure_base = LocalPlayer():Armor() / 100 * 200
    local taille_barre_armure = 0
    if taille_barre_armure_base > 200 then
        taille_barre_armure = 200
    else
        taille_barre_armure = taille_barre_armure_base
    end

    draw.RoundedBox(10, ScrW()/2 + 5, ScrH() - 35, 250, 30, Color(15, 15, 15, 250))-- Fond de la barre

    draw.RoundedBox(15, ScrW()/2 + 17, ScrH() - 27, 200, 15, Color(30, 30, 30, 255)) -- Fond de la barre bleu
    draw.RoundedBox(15, ScrW()/2 + 217 - taille_barre_armure, ScrH() - 27, taille_barre_armure, 15, Color(0, 100, 255, 255)) -- Bleu de la barre d'armure
    draw.SimpleText(LocalPlayer():Armor() or "Error", "DermaDefault", ScrW()/2 + 117, ScrH() - 26, color_white, TEXT_ALIGN_CENTER) -- Nombre armure
    draw.RoundedBox(10, ScrW()/2 + 225, ScrH() - 30, 2, 20, Color(60, 60, 60, 250))-- Barre de séparation

    -- Icon de armure
    surface.SetMaterial(Material("materials/armor_icon.png"))
    surface.SetDrawColor(255,255,255)
    surface.DrawTexturedRect(ScrW()/2 + 229, ScrH() - 30, 20, 20)
end

local function hud_right_part()
    --[[---
    Barre de munitions 
    -----]]

    local arme = LocalPlayer():GetActiveWeapon()
    if IsValid(arme) and (arme:GetPrimaryAmmoType() > 0) then
        draw.RoundedBox(8, ScrW() - 160, ScrH() - 35, 150, 30, Color(15, 15, 15, 250))-- Fond de la barre
        draw.RoundedBox(10, ScrW() - 45, ScrH() - 30, 2, 20, Color(60, 60, 60, 250))-- Barre de séparation

        -- Texte
        draw.SimpleText(arme:Clip1().. " / ".. LocalPlayer():GetAmmoCount(arme:GetPrimaryAmmoType()), "Trebuchet18", ScrW() - 97, ScrH() - 20, arme:Clip1() > 0 and color_white or Color(255, 20, 20, 255), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER)

        -- Icon de munition
        surface.SetMaterial(Material("materials/ammo_icon.png"))
        surface.SetDrawColor(255,255,255)
        surface.DrawTexturedRect(ScrW() - 40, ScrH() - 30, 25, 20)
    end
end

--[[-------------------------------------------------------------------------
                      Load the hud after checking the gamemode
---------------------------------------------------------------------------]]
-- Ask to the server if the gamemode is based on darkrp when netmessage can be sent.
hook.Add("InitPostEntity", "swmbhud_netmessage_ready", function()
	net.Start("swmbhud_ask_for_gamemode")
	net.SendToServer()
end)

-- Load the hud when the server confirmed that the gamemode is based on darkrp.
net.Receive("load_starwars_multi_bar_hud", function(len, ply)
    -- Hide base gmod and darkrp base hud.
    hook.Add("HUDShouldDraw", "hide_hud", function(name)
        if 
            name == "CHudHealth" or 
            name == "CHudBattery" or 
            name == "CHudSuitPower" or 
            name == "CHudAmmo" or 
            name == "CHudSecondaryAmmo" or
            name == "DarkRP_HUD" or 
            name == "DarkRP_LocalPlayerHUD" or
            name == "DarkRP_EntityDisplay" or
            name == "DarkRP_Hungermod"
            then return false
        end
    end)

    -- Add the hud's functions to the HUDPaint hook.
    hook.Add("HUDPaint", "load_starwars_multi_bar_hud", function()
        -- Disable the hud if it not should draw.
        if GetConVar("cl_drawhud"):GetBool() == false then return end
    
        -- Load the hud's parts.
        hud_left_part()
        hud_middle_part()
        hud_right_part()
    end)
end)
