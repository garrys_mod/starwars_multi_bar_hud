util.AddNetworkString("swmbhud_ask_for_gamemode")
util.AddNetworkString("load_starwars_multi_bar_hud")

-- Send to client the green light to load the hud if the gamemode is darkrp.
net.Receive("swmbhud_ask_for_gamemode", function(len, ply)
    if DarkRP then
        net.Start("load_starwars_multi_bar_hud")
            net.Send(ply)
    end
end)
